
from tkinter import *
from tkinter import messagebox
#import MySQLdb           #pip install MySQL-python

class MyFirstGui:
    def __init__(self, master):
        self.master = master

        master.title("A Simple GUI")

        frame = Frame(master)

        frame.pack(side=TOP,fill=X)

        self.lblN = Label(frame, text="Enter your name:")
        self.lblN.pack(side=LEFT)

        self.entryName = Entry(frame)
        self.entryName.pack(side=RIGHT)

        frame2 = Frame(master)
        frame2.pack(side=TOP,fill=X)

        self.lblP = Label(frame2, text="Enter Your Password:")
        self.lblP.pack(side=LEFT)

        self.entryPass = Entry(frame2)
        self.entryPass.pack(side=RIGHT)

        frame3 = Frame(master)
        frame3.pack(side=BOTTOM)

        self.submitinFile = Button(frame3, text="Submit in File", command=self.action)
        self.submitinFile.pack(side=RIGHT)

        self.submitindb = Button(frame3, text="Submit in DB", command=self.actiondb)
        self.submitindb.pack(side=RIGHT)

    def action(self):
        name = self.entryName.get()
        password = self.entryPass.get()

        fo = open("tkiner.txt", "w+" , encoding='utf-8')
        x = fo.write(name+password)

        messagebox.showinfo("", "Data Inserted to file")


    def actiondb(self):
        name = self.entryName.get()
        password = self.entryPass.get()

        db = MySQLdb.connect("localhost", "dbName", "root", "")
        cursor = MySQLdb.curser()
        sql = "INSERT INTO `users` (`name`, `password`) VALUES (%s, %S)"
        try:
            cursor.excute(sql, (name, password))
            db.commit()
        except:
            db.rollback()

        db.close()

root = Tk()
my_gui = MyFirstGui(root)
root.mainloop()

