<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class upload extends Model
{
    //
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = "uploads";
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];


}
