<?php

namespace App\Http\Controllers\Drive;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\upload;
use Illuminate\support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;


class fileController extends Controller
{
    public function getIndex()
    {
        // get all files from db
        $files = upload::all();

        return view('site.pages.drive.index', compact('files'));
    }

    public function upload(Request $request)
    {
        // dd($request->folders);
        // // request files from ajax
        // if($request->files){
        //
        //     // create object from upload Model
        //     $upload = new upload();
        //
        //     foreach ($request->files as $key => $file) {
        //
        //         $file_name = $file->getClientOriginalName();
        //         $file_extenction = $file->getClientOriginalExtension();
        //         $file_path = $file->getRealPath();
        //         $file_size = $file->getSize();
        //
        //         // check if file is exists in storage/uploads folder
        //         if(file_exists('storage/uploads/'.$file_name))
        //         {
        //             return [
        //                 'status' => 'error',
        //                 'title' => 'Error',
        //                 'text' => $file_name.' is exists in db!'
        //             ];
        //         }
        //
        //         // append data in upload Model in db
        //         $drive = $upload->forceCreate([
        //             'name' => $file_name,
        //             'type' => $file_extenction,
        //             'size' => $file_size
        //         ]);
        //
        //         // check if no file uploaded
        //         if(!$drive){
        //             return [
        //                 'status' => 'error',
        //                 'title' => 'Error',
        //                 'text' => 'There is no files uploaded in db!'
        //             ];
        //         }
        //
        //         // save files in storage/uploads destination
        //         $destinationPath = 'storage/uploads';
        //         $file->move($destinationPath,$file_name);
        //     }
        //
        //     // check if status is true
        //     return [
        //         'status' => 'success',
        //         'title' => 'Success',
        //         'text' => 'File has been Uploaded successfully done!'
        //     ];
        // }

        $folders =$request->data;
        dd($folders);
        if ($folders) {

            foreach ($folders as $key => $folder) {

                $folder_name =  dirname($folder) . PHP_EOL;
                $files =  basename($folder) . PHP_EOL;

                // check if file is exists in storage/uploads folder
                if(file_exists('storage/uploads/'.$folder_name))
                {
                    return [
                        'status' => 'error',
                        'title' => 'Error',
                        'text' => $folder_name.' is exists in db!'
                    ];
                }

                // save files in storage/uploads destination
                $destinationPath = 'storage/uploads';
                Storage::MakeDirectory($destinationPath.'/'.$folder_name, $mode = 0777, true);
                $folderName = $destinationPath.'/'.$folder_name;
                Storage::put($files, $destinationPath);
                // $folderName->store($files)
            }

            // check if status is true
            return [
                'status' => 'success',
                'title' => 'Success',
                'text' => 'Folder has been Uploaded successfully done!'
            ];
        }
    }
}
