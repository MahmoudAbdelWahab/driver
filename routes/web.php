<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('site.pages.drive.index');
});

Route::group(['namespace' => 'Drive'], function(){

    Route::get('', 'fileController@getIndex')->name('drive.index');
    Route::post('upload', 'fileController@upload')->name('drive.upload');

});
