<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Drive</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/toastr.min.css') }}">

    @yield('styles')
</head>
<body>
<nav>
    <div id="logo">
        <i class="fas fa-cloud-upload-alt"></i>
        <span class="websiteName">Supload</span>
    </div>
</nav>

<div class="form-group col-sm-6">
    @yield('content-header')
</div>

<div id="body" class="row">

    @include('site.layouts.sidebar')

        @yield('content')

</div>
    <script src="assets/js/jquery-3.3.1.min.js"></script>

<script>

    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
</script>

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/toastr.min.js') }}"></script>

</body>
</html>
