@extends('site.master')

@section('content')

<section class="col-lg-10" id="uploadedFilesAndFolders">
    <h3><i class="far fa-folder-open"></i> Folders</h3>
    <ul id="folders" class="row">
        <!-- <li class="folder col-lg-2"> -->
            <!-- <a href="google.com">
                <div class="folderIcon">
                    <i class="far fa-folder-open"></i>
                </div>
                <span>images</span>
            </a> -->
        <!-- </li>
        <li class="folder col-lg-2">
            <a href="google.com">
                <div class="folderIcon">
                    <i class="far fa-folder-open"></i>
                </div>
                <span>movies</span>
            </a>
        </li>
        <li class="folder col-lg-2">
            <a href="google.com">
                <div class="folderIcon">
                    <i class="far fa-folder-open"></i>
                </div>
                <span>Programs</span>
            </a>
        </li>
        <li class="folder col-lg-2">
            <a href="google.com">
                <div class="folderIcon">
                    <i class="far fa-folder-open"></i>
                </div>
                <span>Courses</span>
            </a>
        </li> -->
    </ul>
    <h3><i class="far fa-file"></i> Files</h3>
    <ul class="row" id="files">

        @foreach($files as $file)
            @if($file->type == 'jpg & png')
                <li class="file col-lg-2">
                    <a href="#">
                        <img src="{{asset('sotrage/uploads/'.$file->name)}}" alt="img">
                        <span>{{$file->name}}</span>
                    </a>
                </li>
            @else
                <li class="file  col-lg-2">
                    <div class="fileIcon">
                        <i class="far fa-file"></i>
                    </div>
                    <span>{{$file->name}}</span>
                </li>
            @endif
        @endforeach
    </ul>
</section>


<script src="assets/js/jquery-3.3.1.min.js"></script>

<script>

    $('#upfile').on('click',function () {

        $("#filein").trigger("click");
    });


    $("#filein").on('change', function (event) {

        files = event.target.files;

        // console.log(files);
        var form_data = new FormData();

        $.each(files, function(key, value)
        {
            form_data.append(key, value);
        });

        var action = $('#fileUpload').attr('action');

        $.ajax({
            url: action,
            type: 'POST',
            data: form_data,
            success:function(data){
                if (data.status == 'success') {
                    toastr["success"](data.text);
                    location.reload(0);
                } else {
                    //error code...
                    toastr["error"](data.text);
                }
            },
            error: function (data) {
                toastr["error"](data.text);
            },
            cache: false,
            processData: false,
            contentType: false,
        });

        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": "toast-bottom-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    });


    $('#upfolder').on('click',function () {

        $("#folderin").trigger("click");
    });

    $("#folderin").on('change', function (event) {

        folders = event.target.files;

        var form_data = new FormData();

        $.each(folders, function(key, value)
        {
            form_data.append(key, value.webkitRelativePath);
        });


        var action = $('#fileUpload').attr('action');

        $.ajax({
            url: action,
            type: 'POST',
            data: form_data,
            success:function(data){
                if (data.status == 'success') {
                    toastr["success"](data.text);
                    location.reload(0);
                } else {
                    //error code...
                    toastr["error"](data.text);
                }
            },
            error: function (data) {
                toastr["error"](data.text);
            },
            cache: false,
            processData: false,
            contentType: false,
        });

        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": true,
          "positionClass": "toast-bottom-right",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    });


</script>

@endsection
