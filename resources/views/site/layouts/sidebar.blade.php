
<aside class="col-lg-2" id="sidebar">
    <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                + New
            </button>
        <form id="fileUpload" action="{{ route('drive.upload') }}" method="POST" enctype="multipart/form-data">

            <input type="file" id="filein" name="file" style="visibility: hidden" multiple>
            <input type="file" id="folderin" name="folder" style="visibility: hidden" webkitdirectory directory multiple/>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" id="upfile" >File </a>
                <a class="dropdown-item" id="upfolder">Folder</a>
            </div>
        </form>
    </div>
    <ul class="items">
        <li><a href="{{ route('drive.index') }}"><i class="far fa-clock"></i>Recent</a></li>
        <li><a href=""><i class="far fa-star"></i>Starred</a></li>
        <li><a href=""><i class="far fa-trash-alt"></i>Bin</a></li>
    </ul>
</aside>
